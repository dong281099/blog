<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function showLoginForm()
    {
        return view('auth.admin_login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        if(Auth::guard('admin')->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])){
            return redirect(route('admin'));
        }else{
            \Session::flash('error', 'Email hoặc mật khẩu không đúng !!!');
            return redirect()->back();
        }
    }
}
